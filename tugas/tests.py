from django.test import TestCase
from django.test.client import Client
from django.apps import apps
from tugas.apps import TugasConfig
from django.http import HttpRequest
from django.urls import resolve
from .views import index



# Create your tests here.
class TestUnit(TestCase):

    def test_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_registration_page_exist(self):
        response = Client().get('/register/')
        self.assertEqual(response.status_code,200)

    def test_template_used(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'pages/index.html')

    def test_app(self):
        self.assertEqual(TugasConfig.name, 'tugas')
        self.assertEqual(apps.get_app_config('tugas').name, 'tugas')

    def test_title_exists(self):
        response = Client().get('/')
        response_content = response.content.decode('utf-8')
        self.assertIn("Hi", response_content)

    def test_resolve_function(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_register_valid(self):
        form = {'username': "yeet", 'first_name': "Yogs", 'last_name': "Lex", 'email': "yeetmeister@gmail.com", 'password1': "testtest321", 'password2': "testtest321"}
        response = self.client.post('/register/', data = form)
        self.assertEqual(response.status_code, 302)

    def test_register_not_valid(self):
        form = {'username': "yeet", 'first_name': "Yogs", 'last_name': "Lex", 'email': "yeetmeister@gmail.com", 'password1': "testtest321", 'password2': "testtest123"}
        response = self.client.post('/register/', data = form)
        self.assertEqual(response.status_code, 200)
