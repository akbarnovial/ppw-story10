function bookSearch() {
  var search = document.getElementById('search').value
  document.getElementById('results').innerHTML = ""
  console.log(search)

  $.ajax({
    url: "https://www.googleapis.com/books/v1/volumes?q=" + search,
    dataType: "json",

    success: function(data){
      results.innerHTML += "<tbody>"
      results.innerHTML += "<tr>" + "<th>" + "Book Name" + "</th>" + "<th>" + "Author" + "</th>" + "<th>" + "Likes" + "</th>" + "</tr>"
      for (i=0; i < data.items.length; i++){
        results.innerHTML += "<tr>" + "<td>" + data.items[i].volumeInfo.title + "</td>" + "<td>" + data.items[i].volumeInfo.authors[0] + "</td>" + "<td><p id='likes" + i + "'>0</p><button id='button" + i + "' onclick=incrementLike('likes" + i + "')>Like</button></td>" + "</tr>"
      }
      results.innerHTML += "</tbody>"
    },

    type: 'GET'
  });
}

document.getElementById('search').addEventListener('keypress', function(e){
  if (e.keyCode == 13) {
    bookSearch();
  }
}, false)
